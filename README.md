# Configuration Files for linux
### Created by Venkatakrishnan Sutharsan

The files to Configure linux for ethernet is in this repository. This has to be copied to the respective directory in order for the compiler to use while making.

The config.txt is the configuration file for linux and should be copied to < linux-devkit path >/work/linux/ as .config

The buildroot_initramfs-menuconfig.txt is the menuconfiguration for buildroot_initramfs and should be copied to < linux-devkit path >/work/buildroot_initramfs/ as .config

The buildroot_rootfs-menuconfig.txt is the menuconfiguration for buildroot_initramfs and should be copied to < linux-devkit path >/work/buildroot_rootfs/ as .config

The dts file should be copied to the following directory < linux-devkit path >/bsp/dts/.

The compilation of the linux is started using the "make bbl" command in the linux-devkit repository.

The BBL currently in the repo contains ethernet enabled and the process to test the BBL is as follows :

1. THe BBL is loaded into the Xilinx Arty-A7 100T using GDB
2. Login as username - root & password - shakti
3. setup the internet configuration using the below commands :
```
	1. ifconfig eth0 down
	2. vi /etc/network/interfaces
	3. auto eth0
		iface eth0 inet static
		address 192.168.1.5
		netmask 255.255.255.0
		gateway 192.168.1.1
	4. Save and exit
	5. vi /etc/resolv.conf
	6. nameserver 8.8.8.8
	   nameserver 8.8.4.4
	7. Save and exit
	8. ifup eth0
```
4. The ethernet can be tested using ping and wget commands.
